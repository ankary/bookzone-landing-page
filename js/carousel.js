$(document).ready(function () {
  $('.carousel').owlCarousel({
    loop: true,
    margin: 170,
    nav: false,
    items: 2,
    responsive: {
      0: {
        items: 1,
        margin: 50
      },
      450: {
        items: 2
      }
    }
  });
});

$(document).ready(function () {
  $('.carousel2').owlCarousel({
    loop: true,
    margin: 0,
    dots: false,
    nav: true,
    items: 3,
    responsive: {
      0: {
        items: 2,
        margin: 50
      },
      450: {
        items: 3
      }
    }
  });
});

